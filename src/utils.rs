use image::DynamicImage;
use image::ImageOutputFormat::Jpeg;
use std::{fs::File, io::BufWriter, path::PathBuf};

/// Function to save the image to file. Uses maximum quality if saved to jpg.
pub fn save_image(img: DynamicImage, output_path: PathBuf) {
    let extension: &str = output_path.extension().unwrap().to_str().unwrap();
    let jpg_vec: Vec<&str> = vec!["jpg", "jpeg"];
    if jpg_vec.iter().any(|&str| str == extension.to_lowercase()) {
        let mut output_buf = BufWriter::new(File::create(&output_path).unwrap());
        img.write_to(&mut output_buf, Jpeg(100))
            .expect("cannot save image");
    } else {
        img.save(output_path).expect("cannot save image");
    }
}

#[cfg(test)]
mod tests {
    use crate::utils::save_image;
    use image::DynamicImage;
    use std::{
        fs,
        path::{Path, PathBuf},
    };

    #[test]
    /// Test that an image is correctly saved.
    fn test_save_image() {
        let img: DynamicImage = DynamicImage::new_rgb8(5, 5);
        let path: &str = "tests/test.jpg";
        let path_buf: PathBuf = path.parse::<PathBuf>().expect("could not create PathBuf");
        save_image(img, path_buf);

        assert!(Path::new(path).exists());
        fs::remove_file(path).expect("could not remove file");
    }

    #[test]
    #[should_panic]
    /// Test that the function panics, if a non-existant path is given.
    fn test_save_image_wrong_path() {
        let img: DynamicImage = DynamicImage::new_rgb8(5, 5);
        let path: &str = "tests/test/test.jpg";
        let path_buf: PathBuf = path.parse::<PathBuf>().expect("could not create PathBuf");
        save_image(img, path_buf);
    }

    #[test]
    #[should_panic]
    /// Test that the function panics, if a wrong extension is given.
    fn test_save_image_wrong_extension() {
        let img: DynamicImage = DynamicImage::new_rgb8(5, 5);
        let path: &str = "tests/test.pdf";
        let path_buf: PathBuf = path.parse::<PathBuf>().expect("could not create PathBuf");
        save_image(img, path_buf);
    }
}
