use clap::{Parser, Subcommand};
use image::{io::Reader as ImageReader, DynamicImage};
use phoden::{bilateral_denoise, identity_denoise, BorderMode};
use std::path::PathBuf;
use strum::Display;
use utils::save_image;

mod utils;

#[derive(Parser, Debug)]
#[command(author, version, about, long_about = None)]
/// CLI arguments.
struct Cli {
    #[arg(short, long)]
    input: PathBuf,

    #[arg(short, long, default_value = "-")]
    output: PathBuf,

    #[command(subcommand)]
    algorithm: Algorithm,
}

#[derive(Subcommand, Debug, Display)]
/// CLI filter subcommands.
enum Algorithm {
    Identity,
    BM3D,
    Bilateral {
        #[arg(short, long, default_value = "0")]
        win_size: u8,

        #[arg(short = 'c', long, default_value = "0")]
        sigma_color: f32,

        #[arg(short, long, default_value = "1")]
        sigma_spatial: f32,

        #[arg(short, long, default_value = "10000")]
        bins: u16,

        #[arg(short = 'm', long, default_value = "constant")]
        border_mode: BorderMode,

        #[arg(short = 'v', long, default_value = "0")]
        border_val: u16,
    },
    Calibrate,
    NLMeans,
    PrimalDual,
    TVBregman,
    TVChambolle,
    Wavelet,
}

fn main() {
    // parse CLI arguments
    let args = Cli::parse();

    // load the image
    let img = ImageReader::open(&args.input)
        .expect("cannot read file")
        .decode()
        .expect("cannot decode image");

    // get filter name
    let filter_name: String = args.algorithm.to_string().to_lowercase();

    // determine output filename
    let mut output: PathBuf = args.output.clone();
    if output.to_str().unwrap() == "-" {
        let parent = args.input.parent().unwrap().to_str().unwrap();
        let filename = args.input.file_stem().unwrap().to_str().unwrap();
        let extension = args.input.extension().unwrap().to_str().unwrap();
        let new_filename = &[filename, &filter_name].join("_");
        output = [parent, &[new_filename, extension].join(".")]
            .iter()
            .collect();
    }

    // apply denoising filter
    println!("Applying {} filter to image {:?}.", filter_name, output);
    let denoised_img = match args.algorithm {
        Algorithm::Identity => identity_denoise(img),
        Algorithm::Bilateral {
            win_size,
            sigma_color,
            sigma_spatial,
            bins,
            border_mode,
            border_val,
        } => bilateral_denoise(
            img,
            win_size,
            sigma_color,
            sigma_spatial,
            bins,
            border_mode,
            border_val,
        ),
        _ => DynamicImage::new_rgb8(5, 5),
    };

    // save the denoised image
    save_image(denoised_img, output)
}
