use image::DynamicImage;
use strum::EnumString;

pub mod utils;

#[derive(Debug, Clone, EnumString)]
pub enum BorderMode {
    #[strum(serialize = "constant")]
    Constant,
    #[strum(serialize = "edgde")]
    Edge,
    #[strum(serialize = "symmetric")]
    Symmetric,
    #[strum(serialize = "reflect")]
    Reflect,
    #[strum(serialize = "wrap")]
    Wrap,
}

/// Identity filter for testing purposes. It returns the image without change.
pub fn identity_denoise(img: DynamicImage) -> DynamicImage {
    img
}

pub fn bilateral_denoise(
    img: DynamicImage,
    win_size: u8,
    sigma_color: f32,
    sigma_spatial: f32,
    bins: u16,
    border_mode: BorderMode,
    border_val: u16,
) -> DynamicImage {
    img
}
