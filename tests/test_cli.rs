use assert_cmd::Command;
use image::imageops::FilterType::Triangle;
use image::io::Reader;
use image::{DynamicImage, Rgb};

use std::fs;
use std::path::Path;

fn pix_diff(pix1: Rgb<u8>, pix2: Rgb<u8>) -> u32 {
    pix1.0
        .into_iter()
        .zip(pix2.0)
        .map(|(a, b)| (a as i32 - b as i32).abs())
        .sum::<i32>() as u32
}

fn img_eq(img1: DynamicImage, img2: DynamicImage, max_rgb_diff: u32) -> bool {
    img1.resize_exact(32, 32, Triangle);
    img2.resize_exact(32, 32, Triangle);
    let diff_num = img1
        .into_rgb8()
        .pixels()
        .zip(img2.into_rgb8().pixels())
        .filter(|(&a, &b)| pix_diff(a, b) > max_rgb_diff)
        .count();
    println!("Number of differing pixel: {}", diff_num);
    diff_num == 0
}

#[test]
fn test_identity_default_output() {
    // define image paths
    let filter_str = "identity";
    let path_original_img = Path::new("tests/fixtures/test_image.jpg");
    let path_result_str = format!("tests/fixtures/test_image_{}.jpg", filter_str);
    let path_result_img = Path::new(path_result_str.as_str());

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("--input")
        .arg(path_original_img)
        .arg(filter_str)
        .assert()
        .success();

    // check if result image was created
    assert!(path_result_img.exists());

    // load test and result image
    let original_img = Reader::open(path_original_img)
        .expect("could not open file")
        .decode()
        .expect("could not decode image");
    let result_img = Reader::open(path_result_img)
        .expect("could not open file")
        .decode()
        .expect("could not decode image");

    // check if images are identical
    assert_eq!(original_img.width(), result_img.width());
    assert_eq!(original_img.height(), result_img.height());
    assert_eq!(original_img.color(), result_img.color());
    assert!(img_eq(original_img, result_img, 10));

    // remove created result image
    fs::remove_file(path_result_img).expect("could not remove file");
}

#[test]
fn test_identity() {
    // define image paths
    let filter_str = "identity";
    let path_original_img = Path::new("tests/fixtures/test_image.jpg");
    let path_result_str = format!("tests/fixtures/test_image_custom_{}.jpg", filter_str);
    let path_result_img = Path::new(path_result_str.as_str());

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("-i")
        .arg(path_original_img)
        .arg("-o")
        .arg(path_result_img)
        .arg(filter_str)
        .assert()
        .success();

    // check if result image was created
    assert!(path_result_img.exists());

    // load test and result image
    let original_img = Reader::open(path_original_img)
        .expect("could not open file")
        .decode()
        .expect("could not decode image");
    let result_img = Reader::open(path_result_img)
        .expect("could not open file")
        .decode()
        .expect("could not decode image");

    // check if images are identical
    assert_eq!(original_img.width(), result_img.width());
    assert_eq!(original_img.height(), result_img.height());
    assert_eq!(original_img.color(), result_img.color());
    assert!(img_eq(original_img, result_img, 10));

    // remove created result image
    fs::remove_file(path_result_img).expect("could not remove file");
}

#[test]
fn test_bilateral_default() {
    // define image paths
    let filter_str = "bilateral";
    let path_original_img = Path::new("tests/fixtures/test_image.jpg");
    let path_result_str = format!("tests/fixtures/test_image_{}.jpg", filter_str);
    let path_result_img = Path::new(path_result_str.as_str());

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("--input")
        .arg(path_original_img)
        .arg(filter_str)
        .assert()
        .success();

    // check if result image was created
    assert!(path_result_img.exists());

    // load test and result image
    let original_img = Reader::open(path_original_img)
        .expect("could not open file")
        .decode()
        .expect("could not decode image");
    let result_img = Reader::open(path_result_img)
        .expect("could not open file")
        .decode()
        .expect("could not decode image");

    // check if images are identical
    assert_eq!(original_img.width(), result_img.width());
    assert_eq!(original_img.height(), result_img.height());
    assert_eq!(original_img.color(), result_img.color());
    assert!(img_eq(original_img, result_img, 10));

    // remove created result image
    fs::remove_file(path_result_img).expect("could not remove file");
}

#[test]
#[should_panic]
fn test_no_input_file() {
    // define image paths
    let filter_str = "identity";
    let path_original_img = Path::new("tests/fixtures/no_test_image.jpg");

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("-i")
        .arg(path_original_img)
        .arg(filter_str)
        .assert()
        .success();
}

#[test]
#[should_panic]
fn test_no_output_folder() {
    // define image paths
    let filter_str = "identity";
    let path_original_img = Path::new("tests/fixtures/test_image.jpg");
    let path_result_str = format!("tests/fixtures/spam/test_image_{}.jpg", filter_str);
    let path_result_img = Path::new(path_result_str.as_str());

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("-i")
        .arg(path_original_img)
        .arg("-o")
        .arg(path_result_img)
        .arg(filter_str)
        .assert()
        .success();
}

#[test]
#[should_panic]
fn test_not_implemented_filter() {
    // define image paths
    let path_original_img = Path::new("tests/fixtures/test_image.jpg");
    let filter_str = "spam";

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("-i")
        .arg(path_original_img)
        .arg(filter_str)
        .assert()
        .success();
}

#[test]
#[should_panic]
fn test_not_implemented_arg() {
    // define image paths
    let filter_str = "identity";
    let path_original_img = Path::new("tests/fixtures/test_image.jpg");

    // check that the test image exists
    assert!(path_original_img.exists());

    // run with identity filter
    Command::cargo_bin("phoden")
        .expect("could not run phoden")
        .arg("--spam")
        .arg("-i")
        .arg(path_original_img)
        .arg(filter_str)
        .assert()
        .success();
}
